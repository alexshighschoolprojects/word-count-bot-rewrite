from nltk.tokenize import RegexpTokenizer
from abc import ABC, abstractmethod
import matplotlib.pyplot as plt
import discord
import imageio
import time
import io
import re


class Processor(ABC):
    def __init__(self, bot, message: discord.Message):
        self.bot = bot
        self.message = message

    async def schedule(self):
        start = time.time()

        # Check if pool is working
        if self.bot.pool_empty():
            await self.bot.send_message(self.message.channel, "**WARNING**: Messages are still being processed, "
                                                              "results may be inaccurate\n")

        # schedule runtime
        self.bot.process.run_task(self.run)

        # run output
        await self.output()

        # timing info
        if time.time() - start < 1:
            await self.bot.send_message(self.message.channel,
                                        "*This query completed in {0:.2f} milliseconds*"
                                        "\n".format((time.time() - start) * 1000))
        else:
            await self.bot.send_message(self.message.channel,
                                        "*This query completed in {0:.2f} seconds*"
                                        "\n".format(time.time() - start))

    @abstractmethod
    async def output(self):
        pass

    @abstractmethod
    def run(self):
        pass


class GetHelp(Processor):
    def __init__(self, bot, message: discord.Message):
        super().__init__(bot, message)
        self.text = ""

    async def output(self):
        command = ''.join(self.message.content.split('!gethelp ')[1:]).lower()

        if 'gethelp' in command:
            output = "`!gethelp [command]` Get usage information about an optional [command]. Leave [command] " \
                     "blank for help on all commands."
        elif 'getwordregex' in command:
            output = "`!getwordregex [regex]` Get all words matching regex [regex]. See http://www.pyregex.com/ " \
                     "for more information on regex's."
        elif 'getbargraph' in command:
            output = "`!getbargraph` Get a bar graph showing the occurrences of the top 20 words."
        elif 'gettop' in command:
            output = "`!gettop [number]` Get the top [number] most common words. [number] defaults to 10"
        elif 'getanimation' in command:
            output = "`!getanimation` Get an animation of bar graphs showing the top 20 word occurrences over time. " \
                     "**Warning: This operation may take a while.**"
        elif 'getchatlog' in command:
            output = "`!getchatlog` Get the entire chat log in csv format."
        elif 'getwordlog' in command:
            output = "`!getwordlog` Get the entire word log in csv format."
        elif 'getfrequency' in command:
            output = "`!getfreqequency` Outputs an animation of the frequency of words over time **Warning: " \
                     "This command may take a while to execute**"
        else:
            output = "**Commands:**\n  • `!gethelp [command]` Outputs information about commands." \
                     "[command] is optional to get more information about a specific command." \
                     "\n  • `!getwordregex [regex]` Outputs all words that match [regex]" \
                     "\n  • `!getbargraph` Outputs a bar graph showing the top most occurring words" \
                     "\n  • `!gettop10` Outputs the top 10 most frequent words" \
                     "\n  • `!getanimation` Outputs an animation of the occurrences of words over time **Warning: " \
                     "This command may take a while to execute**" \
                     "\n  • `!getchatlog` Uploads the entire chat log file in csv format" \
                     "\n  • `!getwordlog` Uploads the entire word log file in csv format" \
                     "\n  • `!getfrequency` Outputs an animation of the frequency of words over time **Warning: " \
                     "This command may take a while to execute**"

        await self.bot.send_message(self.message.channel, output)

    def run(self):
        pass


class GetChatLog(Processor):
    def __init__(self, bot, message: discord.Message):
        super().__init__(bot, message)

    async def output(self):
        await self.bot.send_file(self.message.channel, self.bot.logdb.filename)

    def run(self):
        pass


class GetWordLog(Processor):
    def __init__(self, bot, message: discord.Message):
        super().__init__(bot, message)

    async def output(self):
        await self.bot.send_file(self.message.channel, self.bot.wordcount.filename)

    def run(self):
        pass


class GetTop(Processor):
    def __init__(self, bot, message: discord.Message):
        super().__init__(bot, message)

    async def output(self):
        command = self.message.content.split('!gettop ')
        command = 10 if len(command) == 1 else int(''.join(command[1:]))

        output = "**Out of {} words, the top {} are:**\n".format(self.bot.wordcount.get_count(), command)

        # Add each word
        for index, word in enumerate(self.bot.wordcount.get_ranked()[::-1][:command]):
            output += "  {}. {}: {} occurrence(s)\n".format(index + 1, word[0], word[1])

            # truncate
            while len(strout) > 1500:
                strout = '\n'.join(strout.split('\n')[:-2])

        await self.bot.send_message(self.message.channel, output)

    def run(self):
        pass


class GetBarGraph(Processor):
    def __init__(self, bot, message: discord.Message):
        super().__init__(bot, message)
        self.buf = io.BytesIO()

    async def output(self):
        await self.bot.send_file(self.message.channel, self.buf, filename='plot.png')
        self.buf.close()

    def run(self):
        data = self.bot.wordcount.get_ranked()[::-1][:20][::-1]

        labels = [ii[0][:6] + '..' if len(ii[0]) > 8 else ii[0] for ii in data]
        values = [int(ii[1]) for ii in data]

        plt.barh(range(len(values)), values)
        plt.xlabel("Occurrences")
        plt.ylabel("Word")
        plt.yticks(range(len(labels)), labels)
        plt.tight_layout()

        plt.savefig(self.buf, dpi=72, orientation='landscape', format='png', papertype='legal')
        self.buf.seek(0)

        plt.clf()


class GetWordRegex(Processor):
    def __init__(self, bot, message: discord.Message):
        super().__init__(bot, message)
        self.text = ""

    async def output(self):
        await self.bot.send_message(self.message.channel, self.text)

    def run(self):
        regex = ''.join(self.message.content.split('!getwordregex ')[1:])

        search = re.compile(regex)
        data = self.bot.wordcount.get_ranked()
        output = []

        # apply regex to each entry
        for point in data:
            if search.match(point[0]):
                output.append(point)

        output = output[::-1]

        self.text += "**The results of your regex `{}` are:**\n".format(regex)
        for index, word in enumerate(output):
            self.text += "  {}. {}: {} occurrence(s)\n".format(index + 1, word[0], word[1])

        # truncate
        while len(self.text) > 1500:
            self.text = '\n'.join(self.text.split('\n')[:-2])


class GetAnimation(Processor):
    def __init__(self, bot, message: discord.Message):
        super().__init__(bot, message)
        self.buf = io.BytesIO()

    async def output(self):
        await self.bot.send_file(self.message.channel, self.buf, filename='plot.gif')

    def run(self):
        tok = RegexpTokenizer(r'\w+')

        data = [ii[-1] for ii in self.bot.logdb if len(ii) > 0]
        data = [ii.lower() for ii in data]
        data = [tok.tokenize(ii) for ii in data]

        value = {}
        with imageio.get_writer(self.buf, format='gif', mode='I', subrectangles=True) as writer:
            for index, entry in enumerate(data):
                for word in entry:
                    if word in value.keys():
                        value[word] += 1
                    else:
                        value[word] = 1

                if index % 10 == 0:
                    value_sort = sorted(value.items(), key=lambda x: int(x[1]), reverse=True)

                    labels = [ii[0][:6] + '..' if len(ii[0]) > 8 else ii[0] for ii in value_sort][:20][::-1]
                    values = [int(ii[1]) for ii in value_sort][:20][::-1]

                    plt.barh(range(len(values)), values)
                    plt.xlabel("Occurrences")
                    plt.ylabel("Word")
                    plt.yticks(range(len(labels)), labels)
                    plt.tight_layout()

                    plot = io.BytesIO()
                    plt.savefig(plot, dpi=72, orientation='landscape', format='png', papertype='legal')
                    plot.seek(0)

                    plt.clf()

                    # Add to the animation
                    image = imageio.imread(plot, format='png')
                    writer.append_data(image)

        self.buf.seek(0)


class GetFrequency(Processor):
    def __init__(self, bot, message: discord.Message):
        super().__init__(bot, message)
        self.buf = io.BytesIO()

    async def output(self):
        await self.bot.send_file(self.message.channel, self.buf, filename='plot.gif')

    def run(self):
        tok = RegexpTokenizer(r'\w+')

        data = [ii[-1] for ii in self.bot.logdb if len(ii) > 0]
        data = [ii.lower() for ii in data]
        data = [tok.tokenize(ii) for ii in data]

        value = {}
        with imageio.get_writer(self.buf, format='gif', mode='I', subrectangles=True) as writer:
            for index, entry in enumerate(data):
                for word in entry:
                    if word in value.keys():
                        value[word] += 1
                    else:
                        value[word] = 1

                if index % 100 == 0:
                    value_sort = sorted(value.items(), key=lambda x: int(x[1]), reverse=True)

                    labels = [ii[0][:6] + '..' if len(ii[0]) > 8 else ii[0] for ii in value_sort][:20][::-1]
                    values = [int(ii[1]) for ii in value_sort][:20][::-1]

                    plt.barh(range(len(values)), values)
                    plt.xlabel("Occurrences")
                    plt.ylabel("Word")
                    plt.yticks(range(len(labels)), labels)
                    plt.tight_layout()

                    plot = io.BytesIO()
                    plt.savefig(plot, dpi=72, orientation='landscape', format='png', papertype='legal')
                    plot.seek(0)

                    plt.clf()

                    # Add to the animation
                    image = imageio.imread(plot, format='png')
                    writer.append_data(image)

                    # Clear Words
                    value = {}

        self.buf.seek(0)
