# Use an official Python runtime as a parent image
FROM python:3.6-slim

# Set the working directory
WORKDIR /wordcountbot

# Copy the files over
COPY . /wordcountbot

# Install packages
RUN pip install --trusted-host pypi.python.org -r requirements.txt

# Run the app when the container launches
CMD ["python", "main.py"]
