#!/usr/bin/env bash

echo Enter discord API key:
read apikey
echo $apikey > 'secret.txt'

docker stop wordcountbot
docker rm wordcountbot
docker build -t wordcountbot .
docker run -d --name wordcountbot -v data:/data wordcountbot
