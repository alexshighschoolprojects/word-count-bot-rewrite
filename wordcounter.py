from nltk.tokenize import RegexpTokenizer
from os.path import isfile
from typing import List
import csv


class SimpleDB:
    """ Class to safely store information on non-volatile storage in csv format

    Parameters
    ----------
    filename : str
        The filename of the csv file used to store the database data
    """

    def __init__(self, filename: str):
        self.filename = filename

        # check if file exists
        if not isfile(self.filename):
            open(self.filename, 'w').close()

    def write(self, entry: List) -> None:
        """ Writes a new entry to the db

        Parameters
        ----------
        entry : list
            A list representing a row to add to the end of the database
        """

        with open(self.filename, 'a') as file:
            writer = csv.writer(file, delimiter=',')
            writer.writerow(entry)

    def read(self) -> List[List[str]]:
        """ Reads db info to a list

        Returns
        -------
        list
            A list of lists representing the rows of the database
        """

        with open(self.filename, 'r', encoding='utf8') as file:
            reader = csv.reader(file, delimiter=',')
            lines = list(reader)

        lines = [x for x in lines if x != []]

        return lines

    def update(self, entry: str, value, mode: str = 'a') -> None:
        """ Updates values in db

        Parameters
        ----------
        entry : str
            A string representing the first column of the row containing the entry
        value
            A value that will be added or assigned to the second column of the row containing the entry
        mode : str, optional
            A string selecting the mode. `'a'` is add mode. `'r'` is assign mode.
        """

        f = self.read()

        for index in range(len(f)):
            if f[index][0] == entry:
                if mode == 'a':
                    f[index][1] = int(f[index][1]) + value
                elif mode == 'r':
                    f[index][1] = value

        with open(self.filename, 'w') as file:
            writer = csv.writer(file, delimiter=',')
            writer.writerows(f)

    def auto_add(self, entry: str, value, mode: str = 'a') -> None:
        """ Writes new entry if entry does not exist, updates if it does

        Parameters
        ----------
        entry : str
            A string representing the first column of the row containing the entry
        value
            A value that will be added or assigned to the second column of the row containing the entry
        mode : str, optional
            A string selecting the mode. `'a'` is add mode. `'r'` is assign mode.
        """

        db = self.read()

        if entry in [ii[0] for ii in db]:
            # exists, update
            self.update(entry, value, mode)
        else:
            # does not exist, write
            self.write([entry, value])


class WordCount(SimpleDB):
    """ Class to count words

    Parameters
    ----------
    filename : str
        The filename of the csv file used to store the word count data
    """

    def __init__(self, filename):
        super().__init__(filename)
        self.tok = RegexpTokenizer(r'\w+')

    def add_text(self, text: str) -> None:
        """ Add text to count

        Parameters
        ----------
        text : str
            Text to be added to the word count data
        """

        # tokenize
        words = self.tok.tokenize(text)
        words = [ii.lower() for ii in words]

        # add to db
        for word in words:
            self.auto_add(word, 1)

    def get_ranked(self) -> List[List[str]]:
        """ Gets sorted list of values

        Returns
        -------
        list
            List of words sorted from least to most popular
        """
        return sorted(self.read(), key=lambda ii: int(ii[1]))

    def get_count(self) -> int:
        """ Gets the amount of words that have been recorded

        Returns
        -------
        int
            Count of amount of words entered into database
        """
        return sum(int(ii[1]) for ii in self.read())
