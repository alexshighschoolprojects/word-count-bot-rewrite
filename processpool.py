from multiprocessing.pool import Pool, AsyncResult
import asyncio


class ProcessingPool:
    """ Processing pool class

    Parameters
    ----------
    processes: int, optional
        Number of processes. Recommended to keep at 1.
    """

    def __init__(self, processes: int = 1):
        self._pool = Pool(processes)
        self._pool_result = []

    def is_empty(self) -> bool:
        """ Checks if pool is empty

        Returns
        -------
        bool
            True if pool is empty, false if pool is still processing
        """
        self._pool_result = [prc for prc in self._pool_result if not prc.ready()]
        return len(self._pool_result) != 0

    def run_task(self, func, *args) -> AsyncResult:
        """ Add a task to the pool

        Parameters
        ----------
        func
            Function to execute
        args
            Arguments for `func`
        Returns
        -------
        AsyncResult
            Result of task
        """

        result = self._pool.apply_async(func=func, args=args)
        self._pool_result.append(result)
        return result

    @staticmethod
    async def wait_result(result: AsyncResult, sleep: int = 1) -> None:
        """ Helper method to asynchronously wait for a result

        Parameters
        ----------
        result: AsyncResult
            Result object to wait for result
        sleep: int, optional
            Sleep time. Recommended to keep at 1 second.
        """
        while not result.ready():
            await asyncio.sleep(sleep)
