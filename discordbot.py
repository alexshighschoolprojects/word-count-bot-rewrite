import processpool
import wordcounter
import process
import discord
import asyncio


class DiscordBot(discord.Client):
    def __init__(self):
        super().__init__()

        self.wordcount = wordcounter.WordCount('/data/wordlist.csv')
        self.logdb = wordcounter.SimpleDB('/data/wordlog.csv')

        self.process = processpool.ProcessingPool()

        self.COMMANDS = {
            '!gethelp': process.GetHelp,
            '!getwordregex': process.GetWordRegex,
            '!getbargraph': process.GetBarGraph,
            '!gettop': process.GetTop,
            '!getanimation': process.GetAnimation,
            '!getwordlog': process.GetWordLog,
            '!getchatlog': process.GetChatLog,
            '!getfrequency': process.GetFrequency,
        }

    async def pool_process(self):
        prev_stat = False
        while not self.is_closed:
            if self.process.is_empty() and not prev_stat:
                await self.change_presence(game=discord.Game(name='with your words'))
                prev_stat = True
            elif not self.process.is_empty() and prev_stat:
                await self.change_presence(game=discord.Game(name='the waiting game'))
                prev_stat = False
            await asyncio.sleep(1)

    async def on_ready(self):
        print("Wordbot is Online.")
        print(self.user)

        await self.change_presence(game=discord.Game(name='the waiting game'))
        await asyncio.get_event_loop().create_task(self.pool_process())

    async def on_message(self, message):
        # Log message
        self.process.run_task(
            self.logdb.write, [
                message.timestamp.isoformat(sep=' '),
                message.author.display_name,
                message.author.id,
                message.channel.name,
                message.id,
                message.content
            ]
        )

        # Check if message originates from self
        if message.author != self.user:
            ismatch = False
            await asyncio.sleep(0.1)

            for cmd in self.COMMANDS.items():
                if cmd[0] in message.content:
                    processor = cmd[1](self, message)
                    await processor.schedule()

                    ismatch = True
                    break

            if not ismatch:
                self.process.run_task(self.wordcount.add_text, message.content)
